//Aggregation
//Aggregation in MongoDB is typically done 2-3 steps. Each process/step in aggregation is called a stage.
db.fruits.aggregate([
    // Aggregate documents to get the total stocks of fruits per supplier

    //$match - used to match or get documents that satisfies the condition.
    //Syntax: {$match:{field:<value>}}
    
    {$match:{onSale:true}}, //apple,dragon fruit, banana, mango, kiwi

    //$group - allows us to group together documents and create an analysis out of the grouped documents

    //_id: in the group stage, essentially associates and id to our results.
    //_id: also determines the number of groups.

    //_id: "$supplier" - essentially grouped together documents with the same values in the supplier field.
    //_id:"$<field>" - groups documents based on the value of the indicated field.

    /* 
        $match - apple, kiwi, banana, dragon fruit, mango

        $group = _id -$supplier

        $sum - used to add or total values in a given field

        $sum: "$stocks" -we got the sum of the values of the stocks field of each item per group

        _id: Red Farms

        apple
        supplier - Red Farming
        stocks:20
        dragon fruit
        supplier -Red Farming
        stocks:10

        $sum:30

        _id: Green Farms

        kiwi
        supplier - Green Farming
        stocks:25
        $sum:25

        _id: Yellow Farms

        banana 
        supplier - Yellow Farming
        stocks:15
        mango 
        supplier - Yellow Farming
        stocks:10
        $sum:25
        
    */
    {$group:{_id:"$supplier", totalStocks:{$sum:"$stocks"}}}
])
db.fruits.aggregate([
    {$match:{onSale:true}},
    {$group:{_id:"$supplier", totalStocks:{$sum:"$stocks"},avgPrice:{$avg:"$price"}}}
])

db.fruits.aggregate([
    //What if we add the of _id as null?
    // IF the _id's value is definite or given, $group will only create one group
    {$match:{onSale:true}},
    {$group:{_id:null, totalStocks:{$sum:"$stocks"}}}
])

db.fruits.aggregate([
    {$match:{onSale:true}},
    {$group:{_id:"AllFruits", totalStocks:{$sum:"$stocks"}}}
])

// $match stage is similiar to find(). In fact, you can even add query operators to expand the criteria
db.fruits.aggregate([
    // $match stage is similiar to find(). In fact, you can even add query operators to expand the criteria
    //In fact, you can even add query operators to expand the criteria
    //Mango,Kiwi,Apple
    {$match:{$and:[{onSale:true},{price:{$gte:40}}]}},
    {$group:{_id:"$supplier", totalStocks:{$sum:"$stocks"}}}
])

//$avg - is an operator used in $group stage
//$avg - gets the avg of the values of the given field per group.

//Gets the average stock of fruits per supplier for all item on sale.
db.fruits.aggregate([

    {$match:{onSale:true}},
    {$group:{_id:"$supplier", avgStock:{$avg: "$stocks"}}}

])

db.fruits.aggregate([
    
    {$match:{onSale:true}},
    {$group:{_id:null, avgPrice:{$avg: "$price"}}}

])
//gets the average price of all fruits which supplier is Red Farms Inc
db.fruits.aggregate([
    //Apple,DragonFruit
    {$match:{supplier:"Red Farms Inc."}},
    {$group:{_id:"group1", avgPrice:{$avg: "$price"}}}

])
//$max - will allow us to get the highest value out of all the values in a given field per group.
//Gets the highest number of stock of all fruits on sale
db.fruits.aggregate([
    //Apple,DragonFruit
    {$match:{onSale:true}},
    {$group:{_id:"maxStockOnSale", maxStock:{$max: "$stocks"}}}

])
//Gets the highest price of fruits that are on sale.
db.fruits.aggregate([
  
    {$match:{onSale:true}},
    {$group:{_id:"highestPrice", maxPrice:{$max: "$price"}}}

])
//$min - gets the lowest value of the field per group
db.fruits.aggregate([
  
    {$match:{onSale:true}},
    {$group:{_id:"lowestStockOnSale", minStocks:{$min: "$stocks"}}}

])
//$count - is a stage usually added after $match to count all items that matches our criteria
db.fruits.aggregate([
    //Count All Items on Sale
    //Banana,Apple,Kiwi,Manggo
    {$match:{onSale:true}},
    {$count:"itemsOnSale"}

])
db.fruits.aggregate([

    {$match:{price:{$gt:50}}},
    {$count: "itemsPriceMoreThan50"}

])

db.fruits.aggregate([

  
    {$match:{$and:[{price:{$gte:50}},{stocks:{$lte:20}}]}},
    {$count: "itemsPriceGreaterThan50below20stocks"}

])
//$out - save/outputs the results in a new collection.
//It is usually the last stage in an aggregation pipeline.
//Note: This will overwrite the collection if it already exists.
//The value given for $out becomes the name of the collection to save the result in
db.fruits.aggregate([
  
    {$match:{price:{$lte:50}}},
    {$group:{_id:"$supplier",totalStocks:{$sum:"$stocks"}}},
    {$out:"stocksPerSupplier"}

])
db.fruits.aggregate([
  
    {$match:{onSale:true}},
    {$group:{_id:"$supplier",avgPrice:{$avg:"$price"}}},
    {$out:"avgPricePerSupplier"}

])
//Mini -Activity
//Use mongodb agregation to get the lowest number of stock per supplier for all items on sale.
// Then save the results in a collection called lowestNumberOfStock.
db.fruits.aggregate([
  
    {$match:{onSale:true}},
    {$group:{_id:"$supplier",minStock:{$min:"$stocks"}}},
    {$out:"lowestNumberOfStock"}
])
